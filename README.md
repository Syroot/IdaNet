# IDA.NET

This project (currently a proof-of-concept) loads .NET Core 2 assemblies into IDA 7.0 and allows them to access the native IDA
SDK through an object-oriented .NET library.

You can find detailed information on the [project wiki](https://gitlab.com/Syroot/IdaNet/wikis).

Originally created to learn about the IDA SDK without the need for C++ or Python, and hosting .NET Core.
Slightly inspired by [IDACSharp](https://archive.codeplex.com/?p=idacsharp) and successor of my
[IdaNet.CLI](https://gitlab.com/Syroot/IdaNet.CLI) project.

## Support

You can ask questions and suggest features on Discord aswell. Feel free to [join the Syroot server](https://discord.gg/42M6J5v)!
