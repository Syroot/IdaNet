#if WIN32
#include "Platform.h"

#include <filesystem>
#include <Windows.h>

using namespace std;
using namespace std::experimental::filesystem;

namespace idanet::platform
{
	// ---- METHODS ----------------------------------------------------------------------------------------------------

	wstring GetSystemVariable(wstring const& variableKey)
	{
		wchar_t value[MAX_PATH];
		if (GetEnvironmentVariableW(variableKey.c_str(), value, MAX_PATH) > 0)
		{
			return wstring(value);
		}
		return L"";
	}

	wstring GetExecutableFolder()
	{
		wchar_t moduleFileName[MAX_PATH];
		GetModuleFileNameW(NULL, moduleFileName, MAX_PATH);
		return path(moduleFileName).parent_path();
	}
}
#endif
