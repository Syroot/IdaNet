#include "ClrHost.h"

#include <filesystem>
#include <Windows.h>
#include "ClrHostException.h"
#include "Platform.h"

using namespace std;
using namespace std::experimental::filesystem;

namespace idanet
{
	// ---- CONSTRUCTORS & DESTRUCTOR ----------------------------------------------------------------------------------

	ClrHost::ClrHost(
		wstring const& runtimeFolder,
		vector<wstring> const& assemblyFolders,
		vector<wstring> const& nativeLibFolders)
	{
		// Load the CoreCLR.dll and retrieve the GetCLRRuntimeHost function.
		wstring coreClrFilePath = runtimeFolder / "coreclr.dll";
		HMODULE coreClr = LoadLibraryExW(coreClrFilePath.c_str(), NULL, 0);
		if (!coreClr)
			throw new ClrHostException(L"Could not load CoreCLR.dll.");
		FnGetCLRRuntimeHost fnGetClrRuntimeHost = (FnGetCLRRuntimeHost)GetProcAddress(coreClr, "GetCLRRuntimeHost");
		if (!fnGetClrRuntimeHost)
			throw new ClrHostException(L"Could not find GetCLRRuntimeHost function.");

		// Instantiate and set up a runtime host.
		if (FAILED(fnGetClrRuntimeHost(IID_ICLRRuntimeHost2, (IUnknown**)&_runtimeHost)))
			throw new ClrHostException(L"Could not retrieve ICLRRuntimeHost2 instance.");
		STARTUP_FLAGS startupFlags = static_cast<STARTUP_FLAGS>(
			STARTUP_FLAGS::STARTUP_CONCURRENT_GC
			| STARTUP_FLAGS::STARTUP_SINGLE_APPDOMAIN
			| STARTUP_FLAGS::STARTUP_LOADER_OPTIMIZATION_SINGLE_DOMAIN);
		if (FAILED(_runtimeHost->SetStartupFlags(startupFlags)))
			throw new ClrHostException(L"Could not set runtime host startup flags.");
		if (FAILED(_runtimeHost->Start()))
			throw new ClrHostException(L"Could not start runtime host.");

		// Instantiate the AppDomain with the configured settings.
		int appDomainFlags = APPDOMAIN_ENABLE_PLATFORM_SPECIFIC_APPS
			| APPDOMAIN_ENABLE_PINVOKE_AND_CLASSIC_COMINTEROP
			| APPDOMAIN_DISABLE_TRANSPARENCY_ENFORCEMENT;
		wstring tpaAssembliesPaths = ConcatenatePaths(GetAssembliesFromFolder(runtimeFolder));
		wstring assemblyFoldersPaths = ConcatenatePaths(assemblyFolders);
		wstring nativeLibFoldersPaths = ConcatenatePaths(nativeLibFolders);
		LPCWSTR propertyKeys[] = {
			L"TRUSTED_PLATFORM_ASSEMBLIES",
			L"APP_PATHS",
			L"APP_NI_PATHS",
			L"NATIVE_DLL_SEARCH_DIRECTORIES",
			L"PLATFORM_RESOURCE_ROOTS",
			L"AppDomainCompatSwitch"
		};
		LPCWSTR propertyValues[] = {
			tpaAssembliesPaths.c_str(),
			assemblyFoldersPaths.c_str(),
			assemblyFoldersPaths.c_str(),
			nativeLibFoldersPaths.c_str(),
			assemblyFoldersPaths.c_str(),
			L"UseLatestBehaviorWhenTFMNotSpecified"
		};
		if (FAILED(_runtimeHost->CreateAppDomainWithManager(L"IDA.NET AppDomain", appDomainFlags, NULL, NULL,
			sizeof(propertyKeys) / sizeof(LPCWSTR), propertyKeys, propertyValues, &_appDomainID)))
		{
			throw new ClrHostException(L"Could not create AppDomain.");
		}
	}

	ClrHost::~ClrHost()
	{
		if (_runtimeHost)
		{
			if (_appDomainID)
			{
				_runtimeHost->UnloadAppDomain(_appDomainID, true);
				_appDomainID = 0;
			}
			_runtimeHost->Stop();
			_runtimeHost->Release();
			_runtimeHost = nullptr;
		}
	}

	// ---- METHODS (PUBLIC) -------------------------------------------------------------------------------------------

	void ClrHost::Call(wstring const& assembly, wstring const& type, wstring const& method)
	{
		// Get a delegate for the managed static method.
		void *fnDelegate = NULL;
		HRESULT hr = _runtimeHost->CreateDelegate(_appDomainID, assembly.c_str(), type.c_str(), method.c_str(),
			(INT_PTR*)&fnDelegate);
		if (FAILED(hr))
			throw new ClrHostException(L"Could not retrieve " + type + L"." + method + L" in " + assembly + L".");

		// Invoke the managed method.
		((CallSignature*)fnDelegate)();
	}

	// ---- METHODS (PRIVATE) ------------------------------------------------------------------------------------------

	wstring ClrHost::ConcatenatePaths(vector<wstring> const& paths) const
	{
		wstring concatenatedPaths;
		for (wstring path : paths)
		{
			concatenatedPaths += canonical(path);
			concatenatedPaths += L";";
		}
		return concatenatedPaths;
	}

	vector<wstring> ClrHost::GetAssembliesFromFolder(wstring const& folderPath) const
	{
		vector<wstring> assemblies;
		for (auto& item : directory_iterator(folderPath))
		{
			path path = item.path();

			wstring extension = path.extension();
			transform(extension.begin(), extension.end(), extension.begin(), ::tolower);

			if (extension == L".dll")
				assemblies.push_back(path);
		}
		return assemblies;
	}
}
