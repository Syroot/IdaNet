#include "main.h"

#include <filesystem>
#include "ClrHost.h"
#include "ClrHostException.h"
#include "Platform.h"

using namespace std;
using namespace std::experimental::filesystem;

namespace idanet
{
	// ---- FIELDS -----------------------------------------------------------------------------------------------------

	ClrHost* _clrHost;

	// ---- METHODS ----------------------------------------------------------------------------------------------------

	int idaapi init()
	{
		// Instantiate and run the IDA.NET CLR host.
		try
		{
			wstring idaFolder = platform::GetExecutableFolder();
			wstring userFolder = platform::GetSystemVariable(L"IDAUSR");
			_clrHost = new ClrHost(
				GetRuntimeFolder(idaFolder, userFolder),
				GetAssemblyFolders(idaFolder, userFolder),
				GetNativeLibFolders(idaFolder, userFolder));
			_clrHost->Call(L"Syroot.IdaNet", L"Syroot.IdaNet.Ida", L"IdaInit");
		}
		catch (ClrHostException ex)
		{
			return PLUGIN_SKIP;
		}
		return PLUGIN_KEEP;
	}

	bool idaapi run(size_t arg)
	{
		_clrHost->Call(L"Syroot.IdaNet", L"Syroot.IdaNet.Ida", L"IdaRun");
		return true;
	}

	void idaapi term()
	{
		_clrHost->Call(L"Syroot.IdaNet", L"Syroot.IdaNet.Ida", L"IdaTerm");
		delete _clrHost;
	}

	// ---- Path Tools ----

	wstring GetRuntimeFolder(wstring const& idaFolder, wstring const& userFolder)
	{
		// .NET Core runtime assemblies: IDA>plugins/dotnet/runtime || IDAUSR>plugins/dotnet/runtime
		path currentPath = userFolder / "plugins" / "dotnet" / "runtime";
		if (exists(currentPath))
			return currentPath;

		currentPath = idaFolder / "plugins" / "dotnet" / "runtime";
		if (exists(currentPath))
			return currentPath;

		return L"";
	}

	vector<wstring> GetAssemblyFolders(wstring const& idaFolder, wstring const& userFolder)
	{
		// .NET assembly folders: IDA>plugins/dotnet && IDAUSR>plugins/dotnet
		vector<wstring> folders;

		folders.push_back(idaFolder / "plugins" / "dotnet");

		if (exists(userFolder))
			folders.push_back(userFolder / "plugins" / "dotnet");

		return folders;
	}

	vector<wstring> GetNativeLibFolders(wstring const& idaFolder, wstring const& userFolder)
	{
		// Native library folders: IDA && IDA>plugins && IDAUSR>plugins
		vector<wstring> folders;

		folders.push_back(idaFolder);
		folders.push_back(idaFolder / "plugins");
		folders.push_back(idaFolder / "plugins" / "dotnet");

		if (exists(userFolder))
		{
			folders.push_back(userFolder);
			folders.push_back(userFolder / "plugins");
			folders.push_back(userFolder / "plugins" / "dotnet");
		}

		return folders;
	}
}

plugin_t PLUGIN =
{
	IDP_INTERFACE_VERSION,
	0, // only load with DB for now
	idanet::init,
	idanet::term,
	idanet::run,
	"Syroot IDA.NET", // comment
	"Loads and manages .NET plugins.", // help
	"IDA.NET", // name
	"Alt-F12" // hotkey
};
