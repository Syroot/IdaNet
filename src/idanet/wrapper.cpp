#include "idasdk.h"

#ifdef _WIN32
	#define EXPORT extern "C" __declspec(dllexport)
#elif
	#define EXPORT extern "C"
#endif

// ---- ida.hpp ----

EXPORT idainfo* ida_inf() { return &inf; }

// ---- kernwin.hpp ----

EXPORT void kernwin_error(const char* message) { error(message); }
EXPORT void kernwin_info(const char* message) { info(message); }
EXPORT int kernwin_msg(const char* message) { return msg(message); }
EXPORT void kernwin_warning(const char* message) { warning(message); }

// ---- lines.hpp ----

EXPORT int lines_tag_remove(char* buf, const char* str, int init_level = 0)
{
	qstring qbuf;
	ssize_t result = tag_remove(&qbuf, str, init_level);
	qstrncpy(buf, qbuf.c_str(), MAXSTR);
	return static_cast<int>(result);
}
