#pragma once

#include <stdexcept>

namespace idanet
{
	class ClrHostException : public std::runtime_error
	{
		// ---- FIELDS -------------------------------------------------------------------------------------------------

		private: std::wstring _message;

		// ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

		public: ClrHostException(std::wstring const& message)
			: runtime_error("ClrHostException"), _message(message)
		{
		}

		// ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

		public: std::wstring GetMessage() const
		{
			return _message;
		}
	};
}
