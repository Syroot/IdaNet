#pragma once

#pragma managed(push, off)
#pragma warning(push)
#pragma warning(disable: 4244 4267) // conversion from 'ssize_t' to 'int', possible loss of data
#include <auto.hpp>
#include <bytes.hpp>
#include <demangle.hpp>
#include <entry.hpp>
#include <funcs.hpp>
#include <gdl.hpp>
#include <ida.hpp>
#include <idp.hpp>
#include <kernwin.hpp>
#include <loader.hpp>
#include <name.hpp>
#include <netnode.hpp>
#include <search.hpp>
#include <segment.hpp>
#include <strlist.hpp>
#include <struct.hpp>
#include <xref.hpp>
#pragma warning(pop)
#pragma managed(pop)
