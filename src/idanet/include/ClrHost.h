#pragma once

#include <vector>
#include <xstring>
#include "mscoree.h"

namespace idanet
{
	class ClrHost
	{
		typedef void (__stdcall CallSignature)();

		// ---- FIELDS -------------------------------------------------------------------------------------------------

		private: ICLRRuntimeHost2* _runtimeHost;
		private: unsigned long _appDomainID;

		// ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

		public: ClrHost(
			std::wstring const& runtimeFolder,
			std::vector<std::wstring> const& assemblyFolders,
			std::vector<std::wstring> const& nativeLibFolders);

		public: ~ClrHost();

		// ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

		public: void Call(std::wstring const& assembly, std::wstring const& type, std::wstring const& method);

		// ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

		private: std::wstring ConcatenatePaths(std::vector<std::wstring> const& paths) const;

		private: std::vector<std::wstring> GetAssembliesFromFolder(std::wstring const& folderPath) const;
	};
}
