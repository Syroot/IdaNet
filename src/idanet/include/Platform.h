#pragma once

#include <string>
#include <xstring>

namespace idanet::platform
{
	// ---- METHODS ----------------------------------------------------------------------------------------------------

	std::wstring GetSystemVariable(std::wstring const& variableKey);
	std::wstring GetExecutableFolder();
}