#pragma once

#include <vector>
#include <xstring>
#include "idasdk.h"

namespace idanet
{
	// ---- METHODS ----------------------------------------------------------------------------------------------------

	int idaapi init();
	bool idaapi run(size_t arg);
	void idaapi term();
	
	// ---- Path Tools ----

	std::wstring GetRuntimeFolder(std::wstring const& idaFolder, std::wstring const& userFolder);
	std::vector<std::wstring> GetAssemblyFolders(std::wstring const& idaFolder, std::wstring const& userFolder);
	std::vector<std::wstring> GetNativeLibFolders(std::wstring const& idaFolder, std::wstring const& userFolder);
}
