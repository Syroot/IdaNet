using System;
using Syroot.IdaNet.Database;
using Syroot.IdaNet.Plugins;

namespace Syroot.IdaNet
{
    /// <summary>
    /// Represents an <see cref="Ida"/>-related class providing access to the different application systems.
    /// </summary>
    public abstract class IdaClass : IDisposable
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private bool _disposed;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public IdaClass(Ida ida)
        {
            Ida = ida;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the <see cref="Ida"/> application class hosting all subsystems.
        /// </summary>
        public Ida Ida { get; }

        /// <summary>
        /// Gets the <see cref="IdaDatabase"/> subsystem.
        /// </summary>
        public IdaDatabase Database => Ida.Database;

        /// <summary>
        /// Gets the <see cref="IdaNetPluginLoader"/> subsystem.
        /// </summary>
        public IdaNetPluginLoader NetPluginLoader => Ida.NetPluginLoader;

        /// <summary>
        /// Gets the <see cref="IdaUI"/> subsystem.
        /// </summary>
        public IdaUI UI => Ida.UI;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing).
            Dispose(true);
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
                _disposed = true;
        }
    }
}
