using System;
using Syroot.IdaNet.Database;
using Syroot.IdaNet.Plugins;

namespace Syroot.IdaNet
{
    public class Ida : IDisposable
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const string _outputSeparator = "-----------------------------------------------------------------";

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static Ida _current;

        private bool _disposed;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        private Ida()
        {
            UI = new IdaUI(this);
            UI.WriteLine(_outputSeparator);
            UI.WriteLine("IDA.NET is initializing...");

            Database = new IdaDatabase(this);
            NetPluginLoader = new IdaNetPluginLoader(this);

            UI.WriteLine(_outputSeparator);
        }

        // ---- EVENTS -------------------------------------------------------------------------------------------------

        public event EventHandler Run;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public IdaUI UI { get; }

        public IdaDatabase Database { get; }

        public IdaNetPluginLoader NetPluginLoader { get; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing).
            Dispose(true);
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static void IdaInit()
        {
            _current = new Ida();
        }

        internal static void IdaRun()
        {
            _current.OnRun();
        }

        internal static void IdaTerm()
        {
            _current.Dispose();
            _current = null;
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void OnRun()
        {
            Run?.Invoke(this, EventArgs.Empty);
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    UI.Dispose();
                    Database.Dispose();
                    NetPluginLoader.Dispose();
                }

                _disposed = true;
            }
        }
    }
}
