using System;

namespace Syroot.IdaNet
{
    [Flags]
    public enum AnalysisFlags : uint
    {
        Code = 1 << 0,
        MarkCode = 1 << 1,
        JumpTbl = 1 << 2,
        PurDat = 1 << 3,
        Used = 1 << 4,
        Unk = 1 << 5,
        ProcPtr = 1 << 6,
        Proc = 1 << 7,
        FTail = 1 << 8,
        LVar = 1 << 9,
        StkArg = 1 << 10,
        RegArg = 1 << 11,
        Trace = 1 << 12,
        VerifySP = 1 << 13,
        ANoRet = 1 << 14,
        MemFunc = 1 << 15,
        TrFunc = 1 << 16,
        StrLit = 1 << 17,
        ChkUni = 1 << 18,
        Fixup = 1 << 19,
        DRefOff = 1 << 20,
        ImmOff = 1 << 21,
        DatOff = 1 << 22,
        Flirt = 1 << 23,
        SigCmt = 1 << 24,
        SigMlt = 1 << 25,
        HFlirt = 1 << 26,
        JFunc = 1 << 27,
        NullSub = 1 << 28,
        DoData = 1 << 29,
        DoCode = 1 << 30,
        Final = 1u << 31
    }

    [Flags]
    public enum AnalysisFlags2 : uint
    {
        DoEH = 1 << 0,
        DoRtti = 1 << 1
    }

    public enum FileType : ushort
    {
        ExeOld,
        ComOld,
        Bin,
        Drv,
        Win,
        Hex,
        Mex,
        Lx,
        Le,
        Nlm,
        Coff,
        Pe,
        Omf,
        SRec,
        Zip,
        OmfLib,
        Ar,
        Loader,
        Elf,
        W32Run,
        AOut,
        Prc,
        Exe,
        Com,
        AixAr,
        Macho
    }

    [Flags]
    public enum DatabaseFlags : ushort
    {
        Auto = 1 << 0,
        AllAsm = 1 << 1,
        LoadIdc = 1 << 2,
        NoUser = 1 << 3,
        ReadOnly = 1 << 4,
        ChkOps = 1 << 5,
        NMOps = 1 << 6,
        GraphView = 1 << 7
    }

    [Flags]
    public enum DatabaseMiscFlags : uint
    {
        PcFpp = 1 << 0,
        PcFlat = 1 << 1,
        Is64Bit = 1 << 2,
        IsDll = 1 << 3,
        FlatOffset32 = 1 << 4,
        Msf = 1 << 5,
        WideHbf = 1 << 6,
        DbgNoPath = 1 << 7,
        Snapshot = 1 << 8,
        Pack = 1 << 9,
        Compress = 1 << 10,
        KernMode = 1 << 11
    }

    public enum SpecialSegmentFormat : byte
    {
        Unspecified = 0,
        Use4Bytes = 4,
        Use8Bytes = 8
    }
}
