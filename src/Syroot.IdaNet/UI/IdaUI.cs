using System;
using static Syroot.IdaNet.Sdk.kernwin;

namespace Syroot.IdaNet
{
    public class IdaUI : IdaClass
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        public const char NewLine = '\n';

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public IdaUI(Ida ida) : base(ida) { }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void ExitWithError(string message) => error(message);

        public void ExitWithError(string format, params object[] args) => ExitWithError(String.Format(format, args));

        public void ShowInfo(string message) => info(message);

        public void ShowInfo(string format, params object[] args) => ShowInfo(String.Format(format, args));

        public void ShowWarning(string message) => warning(message);

        public void ShowWarning(string format, params object[] args) => ShowWarning(String.Format(format, args));

        public int Write(string message) => msg(message);

        public int Write(string format, params object[] args) => Write(String.Format(format, args));

        public int WriteLine(string format) => Write(format + NewLine);

        public int WriteLine(string format, params object[] args) => Write(String.Format(format, args) + NewLine);
    }
}
