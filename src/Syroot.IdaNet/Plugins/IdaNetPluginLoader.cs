using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace Syroot.IdaNet.Plugins
{
    /// <summary>
    /// Represents the plugin loader called by IDA to run code in any managed assembly in the plugins/dotnet folder.
    /// </summary>
    public sealed class IdaNetPluginLoader : IdaClass
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private bool _disposed;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        internal IdaNetPluginLoader(Ida ida) : base(ida)
        {
            Ida.Run += _ida_Run;

            // Load plugins from the IDA and user folder.
            List<PluginInfo> plugins = new List<PluginInfo>();
            string wrapperAssemblyFileName = Path.GetFileName(Assembly.GetExecutingAssembly().Location);

            // Add managed assemblies from the IDA plugin folder.
            string idaExeFile = Process.GetCurrentProcess().MainModule.FileName;
            string idaDotnetFolder = Path.Combine(Path.GetDirectoryName(idaExeFile), "plugins", "dotnet");
            plugins.AddRange(LoadPlugins(idaDotnetFolder, wrapperAssemblyFileName));

            // Add managed assemblies from the user plugin folder.
            string userPluginFolder = Path.Combine(Environment.GetEnvironmentVariable("IDAUSR"), "plugins", "dotnet");
            plugins.AddRange(LoadPlugins(userPluginFolder, wrapperAssemblyFileName));

            // Initialize the found plugins and remember them if they initialized correctly.
            Plugins = new List<PluginInfo>();
            foreach (PluginInfo pluginInfo in plugins)
            {
                if (pluginInfo.Instance.Initialize())
                    Plugins.Add(pluginInfo);
            }
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the list of available <see cref="PluginInfo"/> instances.
        /// </summary>
        public IList<PluginInfo> Plugins { get; }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                foreach (PluginInfo pluginInfo in Plugins)
                    pluginInfo.Instance.Dispose();
            }

            _disposed = true;
            base.Dispose(disposing);
        }

        // ---- Helper methods ----

        private IEnumerable<PluginInfo> LoadPlugins(string folderName, string wrapperAssemblyFileName)
        {
            // Ensure the folder exists.
            UI.Write($"Checking plugin folder \"{folderName}\"... ");
            if (!Directory.Exists(folderName))
            {
                UI.WriteLine("Not found.");
            }
            else
            {
                UI.WriteLine("Found.");

                // Iterate through all libraries (ignore the wrapper one itself) and check if it is a plugin.
                foreach (string file in Directory.GetFiles(folderName, "*.dll"))
                {
                    if (Path.GetFileName(file) != wrapperAssemblyFileName)
                    {
                        PluginInfo plugin = LoadPlugin(file);
                        if (plugin != null)
                            yield return plugin;
                    }
                }
            }
        }

        private PluginInfo LoadPlugin(string file)
        {
            UI.Write($"    Checking library \"{Path.GetFileName(file)}\"... ");
            try
            {
                // Go through each public type of the assembly and check if it has a valid Plugin implementation.
                Assembly assembly = Assembly.LoadFile(file);
                foreach (Type type in assembly.ExportedTypes)
                {
                    if (!typeof(Plugin).IsAssignableFrom(type))
                        continue;

                    // Get a PluginAttribute and validate the SDK version.
                    PluginAttribute attribute = type.GetCustomAttribute<PluginAttribute>(false);
                    if (attribute == null)
                        continue;
                    if (attribute.SdkVersion != PluginAttribute.ValidSdkVersion)
                    {
                        throw new PluginException(
                            $"Wrong SDK version {attribute.SdkVersion} (expected {PluginAttribute.ValidSdkVersion}).");
                    }

                    // Instantiate the plugin.
                    Plugin plugin = (Plugin)Activator.CreateInstance(type, new[] { Ida });
                    UI.WriteLine("OK.");
                    return new PluginInfo(plugin, attribute);
                }

                // No implementation found.
                throw new PluginException("Not a plugin.");
            }
            catch (Exception ex)
            {
                // Log error to IDA window.
                UI.WriteLine(ex.Message);
                return null;
            }
        }

        // ---- EVENTHANDLERS ------------------------------------------------------------------------------------------

        private void _ida_Run(object sender, EventArgs e)
        {
            foreach (PluginInfo pluginInfo in Plugins)
                pluginInfo.Instance.Run();
        }
    }
}
