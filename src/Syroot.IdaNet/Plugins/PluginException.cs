using System;
using System.Collections.Generic;
using System.Text;

namespace Syroot.IdaNet.Plugins
{
    internal class PluginException : Exception
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        internal PluginException(string message) : base(message) { }
    }
}
