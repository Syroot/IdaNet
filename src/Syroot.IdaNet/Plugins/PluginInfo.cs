using System;
using System.Collections.Generic;
using System.Text;

namespace Syroot.IdaNet.Plugins
{
    public class PluginInfo
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        internal PluginInfo(Plugin instance, PluginAttribute attribute)
        {
            Instance = instance;
            Attribute = attribute;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public Plugin Instance { get; }

        public PluginAttribute Attribute { get; }
    }
}
