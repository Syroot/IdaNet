namespace Syroot.IdaNet.Plugins
{
    /// <summary>
    /// Represents the abstract base class for any managed IDA plugin.
    /// </summary>
    public abstract class Plugin : IdaClass
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private bool _disposed;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public Plugin(Ida ida) : base(ida) { }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public abstract bool Initialize();

        public abstract void Run();

        public abstract void Terminate();

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected override void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                Terminate();
            }

            _disposed = true;
            base.Dispose(disposing);
        }
    }
}
