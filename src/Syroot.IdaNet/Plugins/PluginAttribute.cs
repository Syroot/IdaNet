using System;

namespace Syroot.IdaNet.Plugins
{
    public class PluginAttribute : Attribute
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        internal const int ValidSdkVersion = 700;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the SDK version this plugin is compiled against.
        /// </summary>
        public int SdkVersion { get; } = ValidSdkVersion;
    }
}
