#pragma warning disable IDE1006 // Naming Styles

using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Syroot.IdaNet
{
    /// <summary>
    /// Represents a range between two addresses. The end address points beyond the range.
    /// </summary>
    [DebuggerDisplay("[{Start,ac,nq} - {End,ac,nq}]")]
    [StructLayout(LayoutKind.Sequential)]
    public struct Range : IComparable, IComparable<Range>, IEquatable<Range>, IFormattable
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        public EA Start;
        public EA End;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public Range(EA start, EA end)
        {
            Start = start;
            End = end;
        }

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        public static bool operator ==(Range a, Range b) => a.CompareTo(b) == 0;

        public static bool operator !=(Range a, Range b) => a.CompareTo(b) != 0;

        public static bool operator >(Range a, Range b) => a.CompareTo(b) > 0;

        public static bool operator <(Range a, Range b) => a.CompareTo(b) < 0;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Clear()
        {
            Start = End = 0;
        }

        public int CompareTo(object obj)
        {
            if (!(obj is Range))
                throw new ArgumentException(nameof(obj));
            return CompareTo((Range)obj);
        }

        public int CompareTo(Range other)
        {
            return Start > other.Start ? 1
                : Start < other.Start ? -1
                : 0;
        }

        public bool Contains(EA ea)
        {
            return Start <= ea && End > ea;
        }

        public bool Contains(Range range)
        {
            return range.Start >= Start && range.End <= End;
        }

        public override bool Equals(object obj)
        {
            return obj is Range && this == (Range)obj;
        }

        public bool Equals(Range other) => this == other;

        public override int GetHashCode()
        {
            int hashCode = -1676728671;
            hashCode = hashCode * -1521134295 + Start.GetHashCode();
            hashCode = hashCode * -1521134295 + End.GetHashCode();
            return hashCode;
        }

        public bool Overlaps(Range range)
        {
            return range.Start < End && Start < range.End;
        }

        public override string ToString()
        {
            return $"[{Start} - {End}]";
        }

        public string ToString(string format, IFormatProvider provider)
        {
            return $"[{Start.ToString(format, provider)} - {End.ToString(format, provider)}]";
        }
    }
}

#pragma warning restore IDE1006 // Naming Styles
