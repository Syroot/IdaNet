#pragma warning disable IDE1006 // Naming Styles

using System;
using System.Diagnostics;

namespace Syroot.IdaNet
{
    /// <summary>
    /// Represents an unsigned integral value using 64-bits size if compiled for IDA 64, otherwise 32-bits, used to
    /// describe selectors.
    /// </summary>
    [DebuggerDisplay("{_value,ac,nq}")]
    public struct Sel : IFormattable
    {
#if EA64
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        public static readonly Sel BadValue = new Sel(UInt64.MaxValue);

        private readonly UInt64 _value;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public Sel(UInt64 value) => _value = value;

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        public static bool operator ==(Sel a, Sel b) => (UInt64)a == (UInt64)b;

        public static bool operator !=(Sel a, Sel b) => (UInt64)a != (UInt64)b;

        public static bool operator >(Sel a, Sel b) => (UInt64)a > (UInt64)b;

        public static bool operator <(Sel a, Sel b) => (UInt64)a < (UInt64)b;

        public static implicit operator Sel(UInt64 value) => new Sel(value);

        public static implicit operator UInt64(Sel value) => value._value;
#else
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        public static readonly Sel BadValue = new Sel(UInt32.MaxValue);

        private readonly UInt32 _value;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public Sel(UInt32 value) => _value = value;

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        public static bool operator ==(Sel a, Sel b) => (UInt32)a == (UInt32)b;

        public static bool operator !=(Sel a, Sel b) => (UInt32)a != (UInt32)b;

        public static bool operator >(Sel a, Sel b) => (UInt32)a > (UInt32)b;

        public static bool operator <(Sel a, Sel b) => (UInt32)a < (UInt32)b;

        public static implicit operator Sel(UInt32 value) => new Sel(value);

        public static implicit operator UInt32(Sel value) => value._value;
#endif

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public bool IsValid => _value != BadValue;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public override bool Equals(object obj)
        {
            return obj == null ? false : _value == ((Sel)obj)._value;
        }

        public override int GetHashCode() => _value.GetHashCode();

        public override string ToString() => _value.ToString();

        public string ToString(string format, IFormatProvider provider) => _value.ToString(format, provider);
    }
}

#pragma warning restore IDE1006 // Naming Styles
