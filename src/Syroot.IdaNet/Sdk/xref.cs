#pragma warning disable IDE1006 // Naming Styles

using System.Runtime.InteropServices;

namespace Syroot.IdaNet.Sdk
{
    /// <summary>
    /// Represents definitions of xref.hpp.
    /// </summary>
    public static class xref
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        public const int XREF_ALL = 0x00;
        public const int XREF_FAR = 0x01;
        public const int XREF_DATA = 0x02;

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        [DllImport(Libs.Ida, EntryPoint = "xrefblk_t_first_from")]
        private static extern bool _xrefblk_t_first_from(ref xrefblk_t xrefblk, EA from, int flags);

        [DllImport(Libs.Ida, EntryPoint = "xrefblk_t_next_from")]
        private static extern bool _xrefblk_t_next_from(ref xrefblk_t xrefblk);

        [DllImport(Libs.Ida, EntryPoint = "xrefblk_t_first_to")]
        private static extern bool _xrefblk_t_first_to(ref xrefblk_t xrefblk, EA to, int flags);

        [DllImport(Libs.Ida, EntryPoint = "xrefblk_t_next_to")]
        private static extern bool _xrefblk_t_next_to(ref xrefblk_t xrefblk);

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        public struct xrefblk_t
        {
            public EA from;
            public EA to;
            public byte iscode;
            public byte type;
            public byte user;

            public bool first_from(EA _from, int flags) => _xrefblk_t_first_from(ref this, _from, flags);

            public bool next_from() => _xrefblk_t_next_from(ref this);

            public bool first_to(EA _from, int flags) => _xrefblk_t_first_to(ref this, _from, flags);

            public bool next_to() => _xrefblk_t_next_to(ref this);

            public bool next_from(EA _from, EA _to, int flags)
            {
                if (first_from(_from, flags))
                {
                    to = _to;
                    return next_from();
                }
                return false;
            }

            public bool next_to(EA _from, EA _to, int flags)
            {
                if (first_to(_to, flags))
                {
                    from = _from;
                    return next_to();
                }
                return false;
            }
        }
    }
}

#pragma warning restore IDE1006 // Naming Styles
