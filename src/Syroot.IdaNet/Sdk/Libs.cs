namespace Syroot.IdaNet.Sdk
{
    /// <summary>
    /// Represents names of native libraries.
    /// </summary>
    internal static class Libs
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        /// <summary>
        /// The name of the IDA SDK library, allowing to call only methods exported through it.
        /// </summary>
#if EA64
        internal const string Ida = "ida64";
#else
        internal const string Ida = "ida";
#endif

        /// <summary>
        /// The name of the IDA.NET plugin, wrapping methods not exported through the IDA SDK.
        /// </summary>
#if EA64
        internal const string IdaNet = "idanet64";
#else
        internal const string IdaNet = "idanet";
#endif
    }
}
