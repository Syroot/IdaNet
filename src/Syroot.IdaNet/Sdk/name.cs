#pragma warning disable IDE1006 // Naming Styles

using System.Runtime.InteropServices;

namespace Syroot.IdaNet.Sdk
{
    /// <summary>
    /// Represents definitions of name.hpp.
    /// </summary>
    public static class name
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        [DllImport(Libs.Ida)]
        public static extern EA get_name_ea(EA from, [MarshalAs(UnmanagedType.LPUTF8Str)] string name);
    }
}

#pragma warning restore IDE1006 // Naming Styles
