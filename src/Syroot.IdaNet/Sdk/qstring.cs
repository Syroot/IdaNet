#pragma warning disable IDE1006 // Naming Styles

using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Syroot.IdaNet.Sdk
{
    /// <summary>
    /// Represents the IDA std::string variant for ASCII strings (defined in pro.h).
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    internal struct qstring
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        internal qvector body;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public override string ToString() => Encoding.ASCII.GetString(body.ToByteArray());
    }

    /// <summary>
    /// Represents the IDA std::string variant for UTF-16 strings (defined in pro.h).
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    internal struct qwstring
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        internal qvector body;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public override string ToString() => Encoding.Unicode.GetString(body.ToByteArray());
    }

    /// <summary>
    /// Represents the IDA std::vector variant (defined in pro.h).
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    internal struct qvector
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        internal IntPtr array;
        internal IntPtr n;
        internal IntPtr alloc;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public byte[] ToByteArray()
        {
            byte[] buffer = new byte[n.ToInt32()];
            Marshal.Copy(array, buffer, 0, buffer.Length);
            return buffer;
        }
    }
}

#pragma warning restore IDE1006 // Naming Styles
