#pragma warning disable IDE1006 // Naming Styles

using System.Runtime.InteropServices;
using System.Text;

namespace Syroot.IdaNet.Sdk
{
    /// <summary>
    /// Represents definitions of lines.hpp.
    /// </summary>
    public static class lines
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public static bool generate_disasm_line(out string buf, EA ea, int flags = 0)
        {
            qstring qbuf = new qstring();
            bool result = _generate_disasm_line(ref qbuf, ea, flags);
            buf = qbuf.ToString();
            return result;
        }

        public static int tag_remove(out string buf, string str, int init_level = 0)
        {
            int result = _tag_remove(MarshalTools.QStringBuilder, str, init_level);
            buf = MarshalTools.QStringBuilder.ToString();
            return result;
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        [DllImport(Libs.Ida, EntryPoint = "generate_disasm_line")]
        private static extern bool _generate_disasm_line(ref qstring buf, EA ea, int flags = 0);

        [DllImport(Libs.IdaNet, EntryPoint = "lines_tag_remove")]
        private static extern int _tag_remove(
            [MarshalAs(UnmanagedType.LPUTF8Str)] StringBuilder buf,
            [MarshalAs(UnmanagedType.LPUTF8Str)] string str,
            int init_level = 0);
    }
}

#pragma warning restore IDE1006 // Naming Styles
