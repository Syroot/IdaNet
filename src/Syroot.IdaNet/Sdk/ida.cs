#pragma warning disable IDE1006 // Naming Styles

using System;
using System.Runtime.InteropServices;

namespace Syroot.IdaNet.Sdk
{
    /// <summary>
    /// Represents definitions of ida.hpp.
    /// </summary>
    public static class ida
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        public const uint AF_CODE = 0x00000001;
        public const uint AF_MARKCODE = 0x00000002;
        public const uint AF_JUMPTBL = 0x00000004;
        public const uint AF_PURDAT = 0x00000008;
        public const uint AF_USED = 0x00000010;
        public const uint AF_UNK = 0x00000020;
        public const uint AF_PROCPTR = 0x00000040;
        public const uint AF_PROC = 0x00000080;
        public const uint AF_FTAIL = 0x00000100;
        public const uint AF_LVAR = 0x00000200;
        public const uint AF_STKARG = 0x00000400;
        public const uint AF_REGARG = 0x00000800;
        public const uint AF_TRACE = 0x00001000;
        public const uint AF_VERSP = 0x00002000;
        public const uint AF_ANORET = 0x00004000;
        public const uint AF_MEMFUNC = 0x00008000;
        public const uint AF_TRFUNC = 0x00010000;
        public const uint AF_STRLIT = 0x00020000;
        public const uint AF_CHKUNI = 0x00040000;
        public const uint AF_FIXUP = 0x00080000;
        public const uint AF_DREFOFF = 0x00100000;
        public const uint AF_IMMOFF = 0x00200000;
        public const uint AF_DATOFF = 0x00400000;
        public const uint AF_FLIRT = 0x00800000;
        public const uint AF_SIGCMT = 0x01000000;
        public const uint AF_SIGMLT = 0x02000000;
        public const uint AF_HFLIRT = 0x04000000;
        public const uint AF_JFUNC = 0x08000000;
        public const uint AF_NULLSUB = 0x10000000;
        public const uint AF_DODATA = 0x20000000;
        public const uint AF_DOCODE = 0x40000000;
        public const uint AF_FINAL = 0x80000000;

        public const uint AF2_DOEH = 0x00000001;
        public const uint AF2_DORTTI = 0x00000002;

        public const ushort f_EXE_old = 0;
        public const ushort f_COM_old = 1;
        public const ushort f_BIN = 2;
        public const ushort f_DRV = 3;
        public const ushort f_WIN = 4;
        public const ushort f_HEX = 5;
        public const ushort f_MEX = 6;
        public const ushort f_LX = 7;
        public const ushort f_LE = 8;
        public const ushort f_NLM = 9;
        public const ushort f_COFF = 10;
        public const ushort f_PE = 11;
        public const ushort f_OMF = 12;
        public const ushort f_SREC = 13;
        public const ushort f_ZIP = 14;
        public const ushort f_OMFLIB = 15;
        public const ushort f_AR = 16;
        public const ushort f_LOADER = 17;
        public const ushort f_ELF = 18;
        public const ushort f_W32RUN = 19;
        public const ushort f_AOUT = 20;
        public const ushort f_PRC = 21;
        public const ushort f_EXE = 22;
        public const ushort f_COM = 23;
        public const ushort f_AIXAR = 24;
        public const ushort f_MACHO = 25;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public static idainfo inf => Marshal.PtrToStructure<idainfo>(ida_inf());

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        [DllImport(Libs.IdaNet)]
        private static extern IntPtr ida_inf();

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        [StructLayout(LayoutKind.Sequential)]
        public struct compiler_info_t
        {
            public comp_t id;
            public cm_t cm;
            public byte size_i;
            public byte size_b;
            public byte size_e;
            public byte defalign;
            public byte size_s;
            public byte size_l;
            public byte size_ll;
            public byte size_ldbl;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct idainfo
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)] public string tag;
            public ushort version;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 16)] public string procname;
            public ushort s_genflags; // DatabaseFlags
            public uint lflags; // DatabaseMiscFlags
            public uint database_change_count;
            public ushort filetype; // FileType
            public ushort ostype;
            public ushort apptype;
            public byte asmtype;
            public byte specsegs; // SpecialSegmentFormat
            public uint af; // AnalysisFlags
            public uint af2; // AnalysisFlags2
            public UVal baseaddr;
            public Sel start_ss;
            public Sel start_cs;
            public EA start_ip;
            public EA start_eat;
            public EA start_sp;
            public EA main;
            public EA min_ea;
            public EA max_ea;
            public EA omin_ea;
            public EA omax_ea;
            public EA lowoff;
            public EA highoff;
            public UVal maxref;
            public Range privrange;
            public SVal netdelta;
            public byte xrefnum;
            public byte type_xrefnum;
            public byte refcmtnum;
            public byte s_xrefflag;
            public ushort max_autoname_len;
            public byte nametype;
            public uint short_demnames;
            public uint long_demnames;
            public byte demnames;
            public byte listnames;
            public byte indent;
            public byte comment;
            public ushort margin;
            public ushort lenxref;
            public uint outflags;
            public byte s_cmtflg;
            public byte s_limiter;
            public short bin_prefix_size;
            public byte s_prefflag;
            public byte strlit_flags;
            public byte strlit_break;
            public byte strlit_zeroes;
            public int strtype;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 16)] public string strlit_pref;
            public UVal strlit_sernum;
            public UVal datatypes;
            public compiler_info_t cc;
            public uint abibits;
            public uint appcall_options;
        }
    }

    public enum cm_t : byte
    {
        Mask = 0xF0,
        Invalid = 0x00,
        Unknown = 0x10,
        VoidArg = 0x20,
        CDecl = 0x30,
        Ellipsis = 0x40,
        StdCall = 0x50,
        Pascal = 0x60,
        FastCall = 0x70,
        ThisCall = 0x80,
        Manual = 0x90,
        Spoiled = 0xA0,
        Reserve4 = 0xB0,
        Reserve3 = 0xC0,
        SpecialE = 0xD0,
        SpecialP = 0xE0,
        Special = 0xF0
    }

    public enum comp_t : byte
    {
        Mask = 0x0F,
        Unknown = 0x00,
        MS = 0x01,
        BC = 0x02,
        Watcom = 0x03,
        Gnu = 0x06,
        VisAge = 0x07,
        BP = 0x08,
        Unsure = 0x80
    }
}

#pragma warning restore IDE1006 // Naming Styles
