#pragma warning disable IDE1006 // Naming Styles

using System;
using System.Runtime.InteropServices;
using Syroot.IdaNet.Utilities;

namespace Syroot.IdaNet.Sdk
{
    /// <summary>
    /// Represents definitions of segment.h.
    /// </summary>
    public static class segment
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        public const int SREG_NUM = 16;

        // segment_t.align
        public const byte saAbs = 0;
        public const byte saRelByte = 1;
        public const byte saRelWord = 2;
        public const byte saRelPara = 3;
        public const byte saRelPage = 4;
        public const byte saRelDble = 5;
        public const byte saRel4K = 6;
        public const byte saGroup = 7;
        public const byte saRel32Bytes = 8;
        public const byte saRel64Bytes = 9;
        public const byte saRelQword = 10;
        public const byte saRel128Bytes = 11;
        public const byte saRel512Bytes = 12;
        public const byte saRel1024Bytes = 13;
        public const byte saRel2048Bytes = 14;
        public const byte saRel_MAX_ALIGN_CODE = saRel2048Bytes;

        // segment_t.comb
        public const byte scPriv = 0;
        public const byte scGroup = 1;
        public const byte scPub = 2;
        public const byte scPub2 = 4;
        public const byte scStack = 5;
        public const byte scCommon = 6;
        public const byte scPub3 = 7;
        public const byte sc_MAX_COMB_CODE = scPub3;

        // segment.perm
        public const byte SEGPERM_EXEC = 1;
        public const byte SEGPERM_WRITE = 2;
        public const byte SEGPERM_READ = 4;
        public const byte SEGPERM_MAXVAL = SEGPERM_EXEC + SEGPERM_WRITE + SEGPERM_READ;

        // segment.bitness
        public const byte SEG_MAX_BITNESS_CODE = 2;

        // segment.flags
        public const ushort SFL_COMORG = 0x01;
        public const ushort SFL_OBOK = 0x02;
        public const ushort SFL_HIDDEN = 0x04;
        public const ushort SFL_DEBUG = 0x08;
        public const ushort SFL_LOADER = 0x10;
        public const ushort SFL_HIDETYPE = 0x20;

        // segment_t.type
        public const byte SEG_NORM = 0;
        public const byte SEG_XTRN = 1;
        public const byte SEG_CODE = 2;
        public const byte SEG_DATA = 3;
        public const byte SEG_IMP = 4;
        public const byte SEG_GRP = 6;
        public const byte SEG_NULL = 7;
        public const byte SEG_UNDF = 8;
        public const byte SEG_BSS = 9;
        public const byte SEG_ABSSYM = 10;
        public const byte SEG_COMM = 11;
        public const byte SEG_IMEM = 12;
        public const byte SEG_MAX_SEGTYPE_CODE = SEG_IMEM;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        [DllImport(Libs.Ida)]
        public static extern int get_segm_qty();

        public static segment_t? getnseg(int n) => MarshalTools.PtrToStructureNullSafe<segment_t>(_getnseg(n));

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        [DllImport(Libs.Ida, EntryPoint = "getnseg")]
        private static extern IntPtr _getnseg(int n);

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        [StructLayout(LayoutKind.Sequential)]
        public struct segment_t
        {
            public Range Range;

            public UVal name;
            public UVal sclass;
            public UVal orgbase;
            public byte align;
            public byte comb;
            public byte perm;

            public byte bitness;
            public bool use32() => bitness >= 1;
            public bool use64() => bitness == 2;
            public int abits() => 1 << (bitness + 4);
            public int abytes() => abits() / 8;

            public ushort flags;
            public bool comorg() => (flags & SFL_COMORG) != 0;
            public void set_comorg() => flags |= SFL_COMORG;
            public void clr_comorg() => flags = (ushort)(flags & ~SFL_COMORG);
            public bool ob_ok() => (flags & SFL_OBOK) != 0;
            public void set_ob_ok() => flags |= SFL_OBOK;
            public void clr_ob_ok() => flags = (ushort)(flags & ~SFL_OBOK);

            public bool is_visible_segm() => (flags & SFL_HIDDEN) == 0;
            public void set_visible_segm(bool visible) => flags.SetFlag(SFL_HIDDEN, !visible);
            public bool is_debugger_segm() => (flags & SFL_DEBUG) != 0;
            public void set_debugger_segm(bool debseg) => flags.SetFlag(SFL_DEBUG, debseg);
            public bool is_loader_segm() => (flags & SFL_LOADER) != 0;
            public void set_loader_segm(bool ldrseg) => flags.SetFlag(SFL_LOADER, ldrseg);
            public bool is_hidden_segtype() => (flags & SFL_HIDETYPE) != 0;
            public void set_hidden_segtype(bool hide) => flags.SetFlag(SFL_HIDETYPE, hide);
            public bool is_ephemeral_segm() => (flags & (SFL_DEBUG | SFL_LOADER)) == SFL_DEBUG;

            public Sel sel;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = SREG_NUM)] public Sel[] defsr;
            public byte type;
            public uint color;
        }
    }
}

#pragma warning restore IDE1006 // Naming Styles
