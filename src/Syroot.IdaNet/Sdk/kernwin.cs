#pragma warning disable IDE1006 // Naming Styles

using System.Runtime.InteropServices;

namespace Syroot.IdaNet.Sdk
{
    /// <summary>
    /// Represents definitions of kernwin.hpp.
    /// </summary>
    public static class kernwin
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------
        /// <summary>
        /// Display error dialog box and exit.<para/>
        /// If you just want to display an error message and let IDA continue, do NOT use this function! Use <see cref="warning"/> or <see cref="info"/> instead.
        /// </summary>
        /// <param name="message">Message string.</param>
        [DllImport(Libs.IdaNet, EntryPoint = "kernwin_error")]
        public static extern void error([MarshalAs(UnmanagedType.LPUTF8Str)] string message);

        /// <summary>
        /// Display info dialog box and wait for the user to press Enter or Esc.<para/>
        /// This messagebox will by default contain a "Don't display this message again" checkbox. If checked, the message will never be displayed anymore (state saved in the Windows registry or the idareg.cfg for a non-Windows version).
        /// </summary>
        /// <param name="message">Message string.</param>
        [DllImport(Libs.IdaNet, EntryPoint = "kernwin_info")]
        public static extern void info([MarshalAs(UnmanagedType.LPUTF8Str)] string message);

        /// <summary>
        /// Output a string to the output window.<para/>
        /// Everything appearing on the output window may be written to a text file. For this the user should define the following environment variable: set IDALOG=idalog.txt
        /// </summary>
        /// <param name="message">Message string.</param>
        /// <returns>Number of bytes output.</returns>
        [DllImport(Libs.IdaNet, EntryPoint = "kernwin_msg")]
        public static extern int msg([MarshalAs(UnmanagedType.LPUTF8Str)] string message);

        /// <summary>
        /// Display warning dialog box and wait for the user to press Enter or Esc.<para/>
        /// This messagebox will by default contain a "Don't display this message again" checkbox if the message is repetitively displayed. If checked, the message won't be displayed anymore during the current IDA session.
        /// </summary>
        /// <param name="message">Message string.</param>
        [DllImport(Libs.IdaNet, EntryPoint = "kernwin_warning")]
        public static extern void warning([MarshalAs(UnmanagedType.LPUTF8Str)] string message);

    }
}

#pragma warning restore IDE1006 // Naming Styles
