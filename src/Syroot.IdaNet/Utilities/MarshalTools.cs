using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Syroot.IdaNet.Sdk
{
    internal static class MarshalTools
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _maxQStringLength = 1024; // MAXSTR

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        internal static readonly StringBuilder QStringBuilder = new StringBuilder(_maxQStringLength, _maxQStringLength);

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        internal static T? PtrToStructureNullSafe<T>(IntPtr ptr)
            where T : struct
        {
            if (ptr == IntPtr.Zero)
                return null;
            else
                return Marshal.PtrToStructure<T>(ptr);
        }
    }
}
