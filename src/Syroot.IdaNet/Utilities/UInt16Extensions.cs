using System;

namespace Syroot.IdaNet.Utilities
{
    /// <summary>
    /// Represents extension methods for <see cref="UInt16"/> instances.
    /// </summary>
    public static class UInt16eExtensions
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Returns an <see cref="UInt16"/> instance represented by the given number of <paramref name="bits"/>, starting
        /// at the <paramref name="firstBit"/>.
        /// </summary>
        /// <param name="self">The extended <see cref="UInt16"/> instance.</param>
        /// <param name="firstBit">The first bit of the encoded value.</param>
        /// <param name="bits">The number of least significant bits which are used to store the <see cref="UInt16"/>
        /// value.</param>
        /// <returns>The decoded <see cref="UInt16"/>.</returns>
        public static UInt16 Decode(this UInt16 self, int firstBit, int bits)
        {
            // Shift to the first bit and keep only the required bits.
            return (UInt16)((self >> firstBit) & ((1u << bits) - 1));
        }

        /// <summary>
        /// Returns the current <see cref="UInt16"/> with the bit at the <paramref name="index"/> set (being 1).
        /// </summary>
        /// <param name="self">The extended <see cref="UInt16"/> instance.</param>
        /// <param name="index">The 0-based index of the bit to enable.</param>
        /// <returns>The current <see cref="UInt16"/> with the bit enabled.</returns>
        public static UInt16 EnableBit(this UInt16 self, int index)
        {
            return (UInt16)(self | (1u << index));
        }

        /// <summary>
        /// Returns the current <see cref="UInt16"/> with the given <paramref name="flag"/> set.
        /// </summary>
        /// <param name="self">The extended <see cref="UInt16"/> instance.</param>
        /// <param name="flag">THe <see cref="UInt16"/> value to enable.</param>
        /// <returns>The current <see cref="UInt16"/> with the bit enabled.</returns>
        public static UInt16 EnableFlag(this UInt16 self, UInt16 flag)
        {
            return (UInt16)(self | flag);
        }

        /// <summary>
        /// Returns the current <see cref="UInt16"/> with the given <paramref name="value"/> set into the given number
        /// of <paramref name="bits"/> starting at <paramref name="firstBit"/>.
        /// </summary>
        /// <param name="self">The extended <see cref="UInt16"/> instance.</param>
        /// <param name="value">The value to encode.</param>
        /// <param name="firstBit">The first bit used for the encoded value.</param>
        /// <param name="bits">The number of bits which are used to store the <see cref="UInt16"/> value.</param>
        /// <returns>The current <see cref="UInt16"/> with the value encoded into it.</returns>
        public static UInt16 Encode(this UInt16 self, UInt16 value, int firstBit, int bits)
        {
            // Clear the bits required for the value and fit it into them by truncating.
            UInt16 mask = (UInt16)(((1u << bits) - 1) << firstBit);
            self &= (UInt16)~mask;
            value = (UInt16)((value << firstBit) & mask);

            // Set the value.
            return (UInt16)(self | value);
        }

        /// <summary>
        /// Returns the current <see cref="UInt16"/> with the bit at the <paramref name="index"/> cleared (being 0).
        /// </summary>
        /// <param name="self">The extended <see cref="UInt16"/> instance.</param>
        /// <param name="index">The 0-based index of the bit to disable.</param>
        /// <returns>The current <see cref="UInt16"/> with the bit disabled.</returns>
        public static UInt16 DisableBit(this UInt16 self, int index)
        {
            return (UInt16)(self & ~(1u << index));
        }

        /// <summary>
        /// Returns the current <see cref="UInt16"/> with the given <paramref name="flag"/> cleared.
        /// </summary>
        /// <param name="self">The extended <see cref="UInt16"/> instance.</param>
        /// <param name="flag">THe <see cref="UInt16"/> value to disable.</param>
        /// <returns>The current <see cref="UInt16"/> with the bit disabled.</returns>
        public static UInt16 DisableFlag(this UInt16 self, UInt16 flag)
        {
            return (UInt16)(self & ~flag);
        }

        /// <summary>
        /// Returns a value indicating whether the bit at the <paramref name="index"/> in the current
        /// <see cref="UInt16"/> is enabled or disabled.
        /// </summary>
        /// <param name="self">The extended <see cref="UInt16"/> instance.</param>
        /// <param name="index">The 0-based index of the bit to check.</param>
        /// <returns><c>true</c> when the bit is set; otherwise <c>false</c>.</returns>
        public static bool GetBit(this UInt16 self, int index)
        {
            return (self & (1 << index)) != 0;
        }

        /// <summary>
        /// Returns the current <see cref="UInt16"/> with all bits rotated in the given <paramref name="direction"/>,
        /// where positive directions rotate left and negative directions rotate right.
        /// </summary>
        /// <param name="self">The extended <see cref="UInt16"/> instance.</param>
        /// <param name="direction">The direction in which to rotate, where positive directions rotate left.</param>
        /// <returns>The current <see cref="UInt16"/> with the bits rotated.</returns>
        public static UInt16 RotateBits(this UInt16 self, int direction)
        {
            int bits = sizeof(UInt16) * 8;
            if (direction > 0)
            {
                return (UInt16)((self << direction) | (self >> (bits - direction)));
            }
            else if (direction < 0)
            {
                direction = -direction;
                return (UInt16)((self >> direction) | (self << (bits - direction)));
            }
            return self;
        }

        /// <summary>
        /// Returns the current <see cref="UInt16"/> with the bit at the <paramref name="index"/> enabled or disabled,
        /// according to <paramref name="enable"/>.
        /// </summary>
        /// <param name="self">The extended <see cref="UInt16"/> instance.</param>
        /// <param name="index">The 0-based index of the bit to enable or disable.</param>
        /// <param name="enable"><c>true</c> to enable the bit; otherwise <c>false</c>.</param>
        /// <returns>The current <see cref="UInt16"/> with the bit enabled or disabled.</returns>
        public static UInt16 SetBit(this UInt16 self, int index, bool enable)
        {
            return enable ? EnableBit(self, index) : DisableBit(self, index);
        }

        /// <summary>
        /// Returns the current <see cref="UInt16"/> with the given <paramref name="flag"/> enabled or disabled,
        /// according to <paramref name="enable"/>.
        /// </summary>
        /// <param name="self">The extended <see cref="UInt16"/> instance.</param>
        /// <param name="flag">THe <see cref="UInt16"/> value to enable or disable.</param>
        /// <param name="enable"><c>true</c> to enable the bit; otherwise <c>false</c>.</param>
        /// <returns>The current <see cref="UInt16"/> with the flag enabled or disabled.</returns>
        public static UInt16 SetFlag(this UInt16 self, UInt16 flag, bool enable)
        {
            return enable ? EnableFlag(self, flag) : DisableFlag(self, flag);
        }

        /// <summary>
        /// Returns the current <see cref="UInt16"/> with the bit at the <paramref name="index"/> enabled when it is
        /// disabled or disabled when it is enabled.
        /// </summary>
        /// <param name="self">The extended <see cref="UInt16"/> instance.</param>
        /// <param name="index">The 0-based index of the bit to toggle.</param>
        /// <returns>The current <see cref="UInt16"/> with the bit toggled.</returns>
        public static UInt16 ToggleBit(this UInt16 self, int index)
        {
            return GetBit(self, index) ? DisableBit(self, index) : EnableBit(self, index);
        }
    }
}
