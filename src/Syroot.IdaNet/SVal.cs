#pragma warning disable IDE1006 // Naming Styles

using System;
using System.Diagnostics;

namespace Syroot.IdaNet
{
    /// <summary>
    /// Represents a signed integral value using 64-bits size if compiled for IDA 64, otherwise 32-bits.
    /// </summary>
    [DebuggerDisplay("{_value,ac,nq}")]
    public struct SVal : IFormattable
    {
#if EA64
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly Int64 _value;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public SVal(Int64 value) => _value = value;

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        public static bool operator ==(SVal a, SVal b) => (Int64)a == (Int64)b;

        public static bool operator !=(SVal a, SVal b) => (Int64)a != (Int64)b;

        public static bool operator >(SVal a, SVal b) => (Int64)a > (Int64)b;

        public static bool operator <(SVal a, SVal b) => (Int64)a < (Int64)b;

        public static implicit operator SVal(Int64 value) => new SVal(value);

        public static implicit operator Int64(SVal value) => value._value;
#else
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly Int32 _value;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public SVal(Int32 value) => _value = value;

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        public static bool operator ==(SVal a, SVal b) => (Int32)a == (Int32)b;

        public static bool operator !=(SVal a, SVal b) => (Int32)a != (Int32)b;

        public static bool operator >(SVal a, SVal b) => (Int32)a > (Int32)b;

        public static bool operator <(SVal a, SVal b) => (Int32)a < (Int32)b;

        public static implicit operator SVal(Int32 value) => new SVal(value);

        public static implicit operator Int32(SVal value) => value._value;
#endif

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public override bool Equals(object obj)
        {
            return obj == null ? false : _value == ((SVal)obj)._value;
        }

        public override int GetHashCode() => _value.GetHashCode();

        public override string ToString() => _value.ToString();

        public string ToString(string format, IFormatProvider provider) => _value.ToString(format, provider);
    }
}
#pragma warning restore IDE1006 // Naming Styles
