using System.Collections.Generic;
using static Syroot.IdaNet.Sdk.lines;
using static Syroot.IdaNet.Sdk.name;
using static Syroot.IdaNet.Sdk.segment;
using static Syroot.IdaNet.Sdk.xref;

namespace Syroot.IdaNet.Database
{
    public class IdaDatabase : IdaClass
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public IdaDatabase(Ida ida) : base(ida) { }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public IEnumerable<Segment> Segments
        {
            get
            {
                int segmentCount = get_segm_qty();
                for (int i = 0; i < segmentCount; i++)
                {
                    segment_t? nativeSegment = getnseg(i);
                    if (!nativeSegment.HasValue)
                        break;
                    yield return new Segment(nativeSegment.Value);
                }
            }
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public EA GetAddress(string name, EA from)
        {
            return get_name_ea(from, name);
        }

        public string GetDisassemblyLine(EA address, bool removeTags = false)
        {
            generate_disasm_line(out string line, address);
            if (removeTags)
                tag_remove(out line, line);
            return line;
        }

        public IEnumerable<Xref> GetXrefsTo(EA address, XrefType types = XrefType.All)
        {
            xrefblk_t xref = new xrefblk_t();
            for (bool ok = xref.first_to(address, (int)types); ok; ok = xref.next_to())
            {
                yield return new Xref(xref);
            }
        }
    }
}
