using static Syroot.IdaNet.Sdk.xref;

namespace Syroot.IdaNet.Database
{
    public class Xref
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private xrefblk_t _nativeXref;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        internal Xref(xrefblk_t nativeXref)
        {
            _nativeXref = nativeXref;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public EA From => _nativeXref.from;

        public EA To => _nativeXref.to;
    }

    public enum XrefType : int
    {
        All = XREF_ALL,
        Far = XREF_FAR,
        Data = XREF_DATA
    }
}
