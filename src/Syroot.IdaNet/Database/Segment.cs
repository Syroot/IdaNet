using static Syroot.IdaNet.Sdk.segment;

namespace Syroot.IdaNet.Database
{
    public class Segment
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private segment_t _nativeSegment;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        internal Segment(segment_t nativeSegment)
        {
            _nativeSegment = nativeSegment;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public Range Range => _nativeSegment.Range;

        public SegmentType Type => (SegmentType)_nativeSegment.type;
    }

    public enum SegmentType : byte
    {
        Normal = SEG_NORM,
        Extern = SEG_XTRN,
        Code = SEG_CODE,
        Data = SEG_DATA,
        Implementation = SEG_IMP,
        Group = SEG_GRP,
        Null = SEG_NULL,
        Undefined = SEG_UNDF,
        Uninitialized = SEG_BSS,
        AbsoluteSymbols = SEG_ABSSYM,
        Communal = SEG_COMM,
        InternalMemory = SEG_IMEM
    }
}
