#pragma warning disable IDE1006 // Naming Styles

using System;
using System.Diagnostics;

namespace Syroot.IdaNet
{
    /// <summary>
    /// Represents an unsigned integral value using 64-bits size if compiled for IDA 64, otherwise 32-bits.
    /// </summary>
    [DebuggerDisplay("{_value,ac,nq}")]
    public struct UVal : IFormattable
    {
#if EA64
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly UInt64 _value;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public UVal(UInt64 value) => _value = value;

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        public static bool operator ==(UVal a, UVal b) => (UInt64)a == (UInt64)b;

        public static bool operator !=(UVal a, UVal b) => (UInt64)a != (UInt64)b;

        public static bool operator >(UVal a, UVal b) => (UInt64)a > (UInt64)b;

        public static bool operator <(UVal a, UVal b) => (UInt64)a < (UInt64)b;

        public static implicit operator UVal(UInt64 value) => new UVal(value);

        public static implicit operator UInt64(UVal value) => value._value;
#else
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly UInt32 _value;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public UVal(UInt32 value) => _value = value;

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        public static bool operator ==(UVal a, UVal b) => (UInt32)a == (UInt32)b;

        public static bool operator !=(UVal a, UVal b) => (UInt32)a != (UInt32)b;

        public static bool operator >(UVal a, UVal b) => (UInt32)a > (UInt32)b;

        public static bool operator <(UVal a, UVal b) => (UInt32)a < (UInt32)b;

        public static implicit operator UVal(UInt32 value) => new UVal(value);

        public static implicit operator UInt32(UVal value) => value._value;
#endif

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public override bool Equals(object obj)
        {
            return obj == null ? false : _value == ((UVal)obj)._value;
        }

        public override int GetHashCode() => _value.GetHashCode();

        public override string ToString() => _value.ToString();

        public string ToString(string format, IFormatProvider provider) => _value.ToString(format, provider);
    }
}

#pragma warning restore IDE1006 // Naming Styles
