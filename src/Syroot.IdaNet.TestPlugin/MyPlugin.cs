using System.Linq;
using Syroot.IdaNet.Database;
using Syroot.IdaNet.Plugins;
using static Syroot.IdaNet.Sdk.ida;
using static Syroot.IdaNet.Sdk.kernwin;
using static Syroot.IdaNet.Sdk.lines;
using static Syroot.IdaNet.Sdk.name;
using static Syroot.IdaNet.Sdk.segment;
using static Syroot.IdaNet.Sdk.xref;

namespace Syroot.IdaNet.TestPlugin
{
    [Plugin]
    public class MyPlugin : Plugin
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public MyPlugin(Ida ida) : base(ida) { }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public override bool Initialize()
        {
            if (inf.filetype != f_ELF && inf.filetype != f_PE)
            {
                error("Executable format must be PE or ELF, sorry.");
                return false;
            }
            return true;
        }

        public override void Run()
        {
            // The functions we're interested in.
            string[] funcs = { "sprintf", "strcpy", "sscanf", };

            // Loop through all segments.
            for (int i = 0; i < get_segm_qty(); i++)
            {
                segment_t seg = getnseg(i).Value;

                // We are only interested in the pseudo segment created by IDA, which is of type SEG_XTRN. This segment
                // holds all function 'extern' definitions.
                if (seg.type == SEG_XTRN)
                {
                    // Loop through each of the functions we're interested in.
                    foreach (string func in funcs)
                    {
                        // Get the address of the function by its name
                        EA loc = get_name_ea(seg.Range.Start, func);
                        // If the function was found, loop through it's referrers.
                        if (loc.IsValid)
                        {
                            msg($"Finding callers to {func} (0x{loc:X8})\n");
                            xrefblk_t xb = new xrefblk_t();
                            // Loop through all the TO xrefs to our function.
                            for (bool ok = xb.first_to(loc, XREF_DATA); ok; ok = xb.next_to())
                            {
                                // Get the instruction (as text) at that address.
                                generate_disasm_line(out string instr, xb.from);
                                tag_remove(out string instr_clean, instr);
                                msg($"Caller to {func}: 0x{xb.from:X8} [{instr_clean}]\n");
                            }
                        }
                    }
                }
            }

            // Iterate over the segments and get the pseudo segment storing function 'extern' definitions.
            foreach (Segment segment in Database.Segments.Where(x => x.Type == SegmentType.Extern))
            {
                // Loop through each of the functions we're interested in.
                foreach (string func in funcs)
                {
                    // Get the address of the function by its name, if available..
                    EA funcAddress = Database.GetAddress(func, segment.Range.Start);
                    if (funcAddress.IsValid)
                    {
                        // Loop through all the to xrefs to our function.
                        UI.WriteLine($"Finding callers to {func} (0x{funcAddress:X8})");
                        foreach (Xref xref in Database.GetXrefsTo(funcAddress, XrefType.Data))
                        {
                            string line = Database.GetDisassemblyLine(xref.From, true);
                            UI.WriteLine($"Caller to {func}: 0x{xref.From:X8} [{line}]");
                        }
                    }
                }
            }
        }

        public override void Terminate()
        {
            Ida.UI.WriteLine("MyPlugin.Terminate");
        }
    }
}
